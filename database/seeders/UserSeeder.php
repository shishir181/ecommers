<?php

namespace Database\Seeders;
use App\Models\User;

use Illuminate\Database\Seeder;

use function GuzzleHttp\Promise\task;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create
        ([
                'name' => 'NUR ALAM SHISHIR',
                'email'=>'adriankhanshishir@gmail.com',
                'usertype'=>'1',
                'password'=>'$2y$10$NwlHzbo6YFmBaOYwkBAprul7BbjtZe6LpKKgX307ZnHa2y5H.6VT2'

        ]);

    }
}
