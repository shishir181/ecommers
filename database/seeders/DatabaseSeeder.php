<?php

namespace Database\Seeders;

use App\Models\Brand;
use App\Models\User;
use App\Models\Product;
use App\Models\Categori;
use App\Models\SubCategory;
use App\Models\UserRole;
use Illuminate\Database\Seeder;
use Database\Seeders\CatagoriSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call([
            AboutSeeder::class,
            UsertypeSeeder::class,
            UserSeeder::class,
            CatagoriSeeder::class,
            SubCategorySeeder::class,
            ColorSeeder::class,
            TagSeeder::class,
            SizeSeeder::class,
            BrandSeeder::class,
            ProductSeeder::class


         ]);



    }
}
