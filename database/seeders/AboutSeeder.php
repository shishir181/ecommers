<?php

namespace Database\Seeders;

use App\Models\About;
use Illuminate\Database\Seeder;

class AboutSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        About::create(
            [

                'details' => ' Bangladesh largest online shop for laptop, Phones, Camera ,Accesories and many more',
                'address' => 'UTTARA DHAKA BANGLADESH',
                'email' => 'shishir@gmail.com',
                'phone' => '01875212323'
            ]
        );
    }
}
